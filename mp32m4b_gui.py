import PySimpleGUI as sg
import subprocess
import os, sys, yaml, ruamel.yaml, pathlib, re, glob, codecs

# In Windows, when started via doubleclick, the current working directory
# is not correct, even when using a shortcut. So we chdir to the script
# location
x = os.chdir( sys.path[0] )

# ###########################################################################
# Functions:
# ###########################################################################

# ---------------------------------------------------------------------------
# Layout for window
# ---------------------------------------------------------------------------
def create_form_main():
   l_main = [ [ sg.Text("Build audio book from MP3 directory") ]
              , [ sg.Text("_" * 80) ]
              , [ sg.Text("MP3 input directory:", size=(18, 1)), sg.InputText(mp3dir, size=(70,1)), sg.FolderBrowse(initial_folder=mp3dir) ]
              , [ sg.Text("M4B output directory:", size=(18, 1)), sg.InputText(m4bdir, size=(70,1)), sg.FolderBrowse(initial_folder=m4bdir) ]
              , [ sg.Text("_" * 80) ]
              , [ sg.Text("Split M4B file to avoid maximum length problem (13h 52m) for Apple devices"), sg.Spin(values=("y", "n"), initial_value=split) ]
              , [ sg.Text("Album Title:", size=(18, 1)), sg.InputText(m4btitle), sg.Submit("Automatic fill title", key="auto_title") ]
              , [ sg.Text("Author:", size=(18, 1)), sg.InputText(m4bauthor), sg.Submit("Automatic fill author", key="auto_author") ]
              , [ sg.Text("Narrator:", size=(18, 1)), sg.InputText(m4bnarrator), sg.Submit("Automatic fill narrator", key="auto_narrator")]
              , [ sg.Text("Cover:", size=(18, 1)), sg.InputText(m4bcover), sg.FileBrowse(file_types=(("Cover", "*.jpg"),("Cover", "*.png")), initial_folder=mp3dir, key="browse_cover")
                                                           , sg.Submit("Extract cover from first MP3 file", key="auto_cover")]
              , [ sg.Text("_" * 80) ]
              , [ sg.Text("\t")
                  , sg.Submit("Convert", key="Convert"), sg.Text("\t")
                  , sg.Submit("Show sequence of MP3 files", key="show"), sg.Text("\t")
                  , sg.Submit("Help", key="help"), sg.Text("\t")
                  , sg.Submit("Cancel", key="cancel")
                ]
            ]
   return(l_main)
   
# ---------------------------------------------------------------------------
# Display help page
# ---------------------------------------------------------------------------
def display_help():
   help = """
_______________________________________________________________________________

mp32m4b is a tool to create a single audio book from a set of MP3 files.
You have to specify:
   - MP3 input directory, all MP3 files will be joined to the audio book
   - M4B output directory, the resulting audio book will be place here
The following additional input can be given (automatic detection is implemented)
   - Title, to specify the audio book title
   - Author, to specify the artist
   - Narrator, to specify the album_artist,
   - Cover, to include a cover picture into the audio book
   
Four buttons are displayed:
   - "Convert" 
      Start audio book creation
   - "Show sequence of MP3 files"
      The MP3 files will be processed in exact the displayed order. The file names
      are expected to include the track number and the track number is the first 
      numeric, space delmited part of the file name 
      (e.g. "My Boook - Chapter 1.mp3")
      If your files are not in the correct order, pls rename the files
   - "Help"
      Display this page
   - "Cancel"
      Abort any further processing
      
Remarks:
   - the GUI will become unreposive while creating the M4B file
   - you MUST press ENTER in the conversion window to finish
   
_______________________________________________________________________________
"""
   sg.popup(help, title="mp32m4b Help")
   return()

# ---------------------------------------------------------------------------
# Store values for next invocation
# ---------------------------------------------------------------------------  
def save_settings():
   # Save settings as yaml file
   out = """\
   config:
      # ###########################################################################
      # Audiobook settings
      # ###########################################################################
      # MP3 Input directory
         mp3dir: %s
      # M4B Output directory
         m4bdir: %s
      # Split output to avoid Apple device problem with audio book >13hours ? (y/n)
         split: %s
      # Audio book title (must be single quoted)
         m4btitle: %s
      # Audio book author (must be single quoted)
         m4bauthor: %s
      # Audio book narrator (must be single quoted)
         m4bnarrator: %s
      # Cover
         m4bcover: %s
   """
   out = out % (mp3dir, \
                m4bdir, \
                split, \
                m4btitle,\
                m4bauthor, \
                m4bnarrator,\
                m4bcover,
                )
   data = ruamel.yaml.round_trip_load(out, preserve_quotes=True)
   settings = ruamel.yaml.round_trip_dump(data)
   with open(settingsFilename, mode="wt", encoding="utf-8") as f:
       ruamel.yaml.round_trip_dump(data, f, width=4096)
   return(True)

# ---------------------------------------------------------------------------
# Search for a suitable cover
# ---------------------------------------------------------------------------
def get_cover():
   if os.path.isfile(mp3dir + os.sep + "cover.png"):
      return(mp3dir + os.sep + "cover.png")
   if os.path.isfile(mp3dir + os.sep + "cover.jpg"):
      return(mp3dir + os.sep + "cover.jpg")
   if os.path.isfile(mp3dir + os.sep + "folder.png"):
      return(mp3dir + os.sep + "folder.png")
   if os.path.isfile(mp3dir + os.sep + "folder.jpg"):
      return(mp3dir + os.sep + "folder.jpg")
   files = glob.glob(mp3dir + "/*.mp3")
   # No cover found, try to extract from first MP3 input file
   # ffmpeg.exe -loglevel warning -i infile -an -vcodec copy cover.png
   command = [ ffmpeg, "-y", "-loglevel", "warning", "-i", files[0], "-an", "-vcodec", "copy", mp3dir + os.sep + "cover.png" ]
   result = subprocess.call(command, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, encoding='utf-8', universal_newlines=True)
   # Check if we got a cover
   if os.path.isfile(mp3dir + os.sep + "cover.png"):
      return(mp3dir + os.sep + "cover.png")
   else:
      return(None)

# ---------------------------------------------------------------------------
# Check input values
# ---------------------------------------------------------------------------
def check_input():
   rc = 0
   if not os.path.isdir(mp3dir):
      rc +=1
      sg.popup_error("mp3dir " + mp3dir + " does not exist or is not a directory", line_width=80)
   elif not os.path.isdir(m4bdir):
      rc +=1
      sg.popup_error("m4bdir " + m4bdir + " does not exist or is not a directory", line_width=80)
   elif not split in ("y", "n"):
      rc +=1
      sg.popup_error("split " + split + r" must be 'y' or 'n'", line_width=80)
   elif m4btitle == "None" or m4btitle == None or m4btitle == "":
      rc +=1
      sg.popup_error("m4btitle " + m4btitle + r" must be filled", line_width=80)  
   files = glob.glob(mp3dir + "/*.mp3")
   if len(files) == 0:
      rc +=1
      sg.popup_error("No MP3 files found in", mp3dir) 
   return(rc)

# ---------------------------------------------------------------------------
# extract title from first MP3 file
# ---------------------------------------------------------------------------      
def get_title():
   command = [ ffprobe, "-loglevel", "warning", "-show_entries", "format_tags=album", "-of", "csv", "-i", files[0] ]
   output = subprocess.check_output(command, stderr=subprocess.STDOUT, encoding='utf-8', universal_newlines=True)
   m4btitle = output.split(",", 1)[1].replace(':'," _").replace("\n", "").replace("\r", "")
   return(m4btitle)

# ---------------------------------------------------------------------------
# extract author from first MP3 file
# ---------------------------------------------------------------------------      
def get_author():   
   command = [ ffprobe, "-loglevel", "warning", "-show_entries", "format_tags=artist", "-of", "csv", "-i", files[0] ]
   output = subprocess.check_output(command, stderr=subprocess.STDOUT, encoding='utf-8', universal_newlines=True)
   m4bauthor = output.split(",", 1)[1].replace(':'," _").replace("\n", "").replace("\r", "")
   return(m4bauthor)

# ---------------------------------------------------------------------------
# extract narrator from first MP3 file
# ---------------------------------------------------------------------------      
def get_narrator():   
   command = [ ffprobe, "-loglevel", "warning", "-show_entries", "format_tags=album_artist", "-of", "csv", "-i", files[0] ]
   output = subprocess.check_output(command, stderr=subprocess.STDOUT, encoding='utf-8', universal_newlines=True)
   m4bnarrator = output.split(",", 1)[1].replace(':'," _").replace("\n", "").replace("\r", "")
   return(m4bnarrator)
   
# ---------------------------------------------------------------------------
# Show list of MP3 files in processing order
# ---------------------------------------------------------------------------      
def show_files():
   # get the list of files
   files1 = glob.glob(mp3dir + os.sep + "*.mp3")
   # The path name must be stripped before sorting because of slash and backslash making problems
   files2 = []
   for i in iter(files1):
      x = os.path.basename(i)
      files2.append(x)
   files2.sort(key=lambda f: int(re.sub(r'[.]', '',re.search(r'(^|\s)\d{1,}(\s|$|[.])', f + ' 999').group())))
   # sg.Table needs a list of lists e.g. [ [ "01","02"] , [ "11", "22"] ]
   # the out list are the rows, the inner list that values per column
   # sg.popup_scrolled(files1, title="MP3 processing sequence", size=(150,20) )
   files = []
   maxl = 0
   for i in iter(files2):
      x = [os.path.basename(i)]
      maxl=max(len(x[0]),maxl)
      files.append(x)
   l1 = [[sg.Table(values=files,
                     headings=["MP3 processing sequence"],
                     max_col_width=maxl-22,
                     auto_size_columns=True,
                     justification='left',
                     # alternating_row_color='lightblue',
                     num_rows=25)]]
   sg.set_options(element_padding=(0, 0))
   w1 = sg.Window('Table', l1, grab_anywhere=False, resizable=True)
   event, values = w1.read()
   window.close()
   del w1
   del l1
   return()
   
# ---------------------------------------------------------------------------
# Create M4B output file
# ---------------------------------------------------------------------------      
def convert():
   global mp3dir, m4bdir, split, m4btitle, m4bauthor, m4bnarrator
   save_settings()
   # run audiobook creation
   sg.popup_quick_message("pls. wait for M4B creation finished. \n" +
                          "Don't care for 'not responding' message \n" +
                          "See the main window for messages and progress.\n" +
                          "press ENTER in the main window when asked ", 
                          auto_close=False, no_titlebar=False, title="M4B creation in progess",
                          location=(10,10))
   import mp32m4b
   return(True)
   
# ###########################################################################
# End Functions:
# ###########################################################################

## retrieve settings for utilities
settingsFilename = "mp32m4b_utils.yaml"
with open(settingsFilename, encoding="utf-8") as cfgfile:
   config = yaml.safe_load(cfgfile)

ffmpeg_path = str(config["config"]["ffmpeg_path"])
kid3_path = str(config["config"]["kid3_path"])

## retrieve settings
settingsFilename = "mp32m4b.yaml"
with open(settingsFilename, encoding="utf-8") as cfgfile:
   config = yaml.safe_load(cfgfile)

mp3dir = str(config["config"]["mp3dir"])
m4bdir = str(config["config"]["m4bdir"])
split = str(config["config"]["split"])
m4btitle = str(config["config"]["m4btitle"])
m4bauthor = str(config["config"]["m4bauthor"])
m4bnarrator = str(config["config"]["m4bnarrator"])
m4bcover = str(config["config"]["m4bcover"])

# ffmpeg_path = r"C:\Users\morqu\Portable\ffmpeg\bin"
ffmpeg = ffmpeg_path + r"\ffmpeg.exe"
ffprobe = ffmpeg_path + r"\ffprobe.exe"
   
finished = False
while not finished:
   l_main = create_form_main()
   window = sg.Window("Convert MP3 to M4B audio book", l_main)
   button, values = window.read()
   # print(button, values)
   if button != None:
      mp3dir = str(values.get(0))
      m4bdir = str(values.get(1))
      split = str(values.get(2))
      m4btitle = str(values.get(3))
      m4bauthor = str(values.get(4))
      m4bnarrator = str(values.get(5))
      m4bcover = str(values.get(6))
      files = glob.glob(mp3dir + "/*.mp3")
      if len(files) == 0:
         print("No AAX files in", mp3dir)
         print("\tPress <ENTER> to continue:")
         sg.popup_error("No MP3 files found in", mp3dir)
   if button == "Convert":
      if convert():
         finished = True
   elif button == "auto_title":
      m4btitle = get_title()
      window.Close()
   elif button == "auto_author":
      m4bauthor = get_author()
      window.Close()
   elif button == "auto_narrator":
      m4bnarrator = get_narrator()
      window.Close()
   elif button == "auto_cover":
      m4bcover = get_cover()
      window.Close()
   elif button == "show":
      show_files()
      window.Close()
   elif button == "help":
      display_help()
      window.Close()
   elif button == "cancel":
      window.Close()
      del window
      del l_main
      exit(1)
   elif button == None:
      # Abort
      # print("Window closed")
      finished = True
   else:
      # should never be reached
      sg.popup_error("This should never happen")
   # window.Close()
   del window
   del l_main
      
# Save settings as yaml file
save_settings()
      
exit()


exit



layout = [[sg.Text("Input:")],
          [sg.Input(do_not_clear=False)],
          [sg.Button("Read"), sg.Exit()],
          [sg.Text("Alternatives:")],
          [sg.Listbox(values=("value1", "value2", "value3"), size=(30, 2), key="_LISTBOX_")]]
#! /usr/bin/env python

import sys, getopt, os, glob, re, pathlib, subprocess, time, shutil, yaml, datetime
from subprocess import *
from pathlib import *

# In Windows, when started via doubleclick, the current working directory
# is not correct, even when using a shortcut. So we chdir to the script
# location
x = os.chdir( sys.path[0] )

# Utilities needed
settingsFilename = "mp32m4b_utils.yaml"
with open(settingsFilename, encoding="utf-8") as cfgfile:
   config = yaml.safe_load(cfgfile)
ffmpeg_path = str(config["config"]["ffmpeg_path"])
kid3_path = str(config["config"]["kid3_path"])
ffmpeg = ffmpeg_path + r"\ffmpeg.exe"
ffprobe = ffmpeg_path + r"\ffprobe.exe"
kid3cli = kid3_path + r"\kid3-cli.exe"

## retrieve settings
settingsFilename = "mp32m4b.yaml"
with open(settingsFilename, encoding="utf-8") as cfgfile:
   config = yaml.safe_load(cfgfile)

mp3dir = str(config["config"]["mp3dir"])
m4bdir = str(config["config"]["m4bdir"])
split = str(config["config"]["split"])
m4btitle = str(config["config"]["m4btitle"])
m4bauthor = str(config["config"]["m4bauthor"])
m4bnarrator = str(config["config"]["m4bnarrator"])
m4bcover = str(config["config"]["m4bcover"])

inputdir = mp3dir
cover = m4bcover
titel = m4btitle
# cover needs to be formatted as URL !!!
if not cover in (None, "None", ""):
   cover=pathlib.Path(cover).as_uri()

# Temporary files will be created in TEMP directory
temp = os.getenv("TMP")
pid = str(os.getpid())
md_file = os.path.join(temp, "mp32m4b" + pid + ".tmp")
in_file = os.path.join(temp, "mp32m4b" + pid + ".in")
outfile = os.path.join(temp, "mp32m4b" + pid + ".m4a")
# print(md_file, in_file)

# get the list of files   
files1 = glob.glob(inputdir + os.sep + "*.mp3")
if len(files1) == 0:
   print("No MP3 files in", inputdir)
   print("\tPress <ENTER> to continue:")
   input()
   exit(False)
# The path name must be stripped before sorting because of slash and backslash making problems
files2 = []
for i in iter(files1):
   x = os.path.basename(i)
   files2.append(x)
files2.sort(key=lambda f: int(re.sub(r'[.]', '',re.search(r'(^|\s)\d{1,}(\s|$|[.])', f + ' 999').group())))
# for ffmpeg the path must be present, so we add it after sorting
files = []
for i in iter(files2):
   x = mp3dir + os.sep + i
   files.append(x)

# Prepare list of input files
try:
   infiles = open(in_file, mode="w")
except OSError:
   print("Failed opening temporary file", in_file, "for output")
   exit(False)
# Prepare metadata file
try:
   md = open(md_file, mode="w", encoding="utf-8")
except OSError:
   print("Failed opening temporary file", md_file, "for output")
   exit(False)
md.write(r";FFMETADATA" + "\n")
md.write(r"title=" + m4btitle + "\n")
md.write(r"artist=" + m4bauthor + "\n")
md.write(r"album_artist=" + m4bnarrator + "\n")
md.write(r"album=" + m4btitle + "\n")
md.write(r"genre=AudioBook" + "\n")

# sort filenames by first numeric part
# files.sort(key=lambda f: int(re.sub('\D', '', f))) 
num = 0
chapters = []     
Start=0
End=0
for file in files:
   num += 1
   command = [ ffprobe, file, "-show_entries", "format=duration", "-show_entries", "format_tags=title", "-v", "error", "-of", "csv=p=0" ]
   output = ""
   # ffprobe checks the activation bytes ever only the chapters are printed.
   # I found no other way to check the activation bytes
   try:
      output = subprocess.check_output(command, stderr=subprocess.STDOUT, encoding='utf-8', universal_newlines=True)
   except subprocess.CalledProcessError:
      print(command)
      print(output)
      print("ffprobe error, press any key to abort")
      md.close()
      infile.close()
      input()
      exit(False)
   # print(output)
   # length of track displayed from ffprobe is in seconds (e.g. 120.222000), ffmpeg needs milliseconds
   length = float(output.split(",", 1)[0])
   title = output.split(",", 1)[1]
   
   # print("Length",length,"Title",title)
   
   length = int(length*1000)
   formnum = str(num).zfill(3)
   
   # Write Chapter information
   md.write(r"[CHAPTER]" + "\n")
   md.write(r"TIMEBASE=1/1000" + "\n")
   md.write(r"START=" + str(Start) + "\n")
   md.write(r"END=" + str(Start+length) + "\n")
   md.write(r"title=" + formnum + " - " + m4btitle + "\n")
   md.write(r"#chapter duration " + str(datetime.timedelta(milliseconds=length)) + "\n")
   Start = Start + length + 1
   # Write list of files
   infiles.write(r"file '" + str(pathlib.PureWindowsPath(file)) + r"'" + "\n")
   
   #chapter duration 00:11:14
   
   # chapters.append([{ "name": formnum + " - " + title, "length": length }])
   
md.close()
infiles.close()

# ffmpeg -f concat -safe 0 -i in_file -i md_file -map_metadata 1 -vn -y -qscale:a 2 -acodec aac -ac 2 -loglevel warning -stats test.m4a
#        |         |       |          |          |               |   |  |           |           |     |                 !      !
#        |         |       |          |          |               |   |  |           |           |     |                 !      (output file)
#        |         |       |          |          |               |   |  |           |           |     |                 !      !
#        |         |       |          |          |               |   |  |           |           |     |                 (progress indicator)
#        |         |       |          |          |               |   |  |           |           |     |
#        |         |       |          |          |               |   |  |           |           |     (Only warnings to be displayed)
#        |         |       |          |          |               |   |  |           |           |
#        |         |       |          |          |               |   |  |           |           (2 channel stereo)
#        |         |       |          |          |               |   |  |           (use internam aac codec)
#        |         |       |          |          |               |   |  (use highest aac quality, try also -vbr or -b:a)
#        |         |       |          |          |               |   (override existing files)
#        |         |       |          |          |               (no video stream)
#        |         |       |          |          (take metadata from second -i file)
#        |         |       |          (file containing metadata)
#        |         |       (list of mp3 files)
#        |         (accept all file names)
#        (concatenate input files)

# Encode the mp3 files to one m4a file
command = [ ffmpeg, "-f", "concat",\
                    "-safe", "0",\
                    "-i", in_file,\
                    "-i", md_file,\
                    "-map_metadata", "1",\
                    "-vn", "-y",\
                    "-qscale:a", "2",\
                    "-acodec", "aac",\
                    "-ac", "2",\
                    "-loglevel", "error",\
                    "-stats",\
                    outfile ]
# Create M4B file 
output = subprocess.call(command, stderr=subprocess.STDOUT, encoding='utf-8', universal_newlines=True)
if not output == 0:
   print("Command executed was" + " ".join(command))
   print("M4B creation failed, pls. check the output")

# Add cover to output file
if not cover in (None, "None", ""):
   command = [ kid3cli, "-c", "albumart " + cover, outfile ]
   output = subprocess.call(command, stderr=subprocess.STDOUT, encoding='utf-8', universal_newlines=True)
   if not output == 0:
      print("Command executed was" + " ".join(command))
      print("M4B cover insert failed, pls. check output")

# Housekeeping
os.remove(md_file)
os.remove(in_file)
# Move temporary M4B file to destination directory
try:
   os.remove(inputdir + os.sep + titel + ".m4b") 
except FileNotFoundError:
   pass
os.rename(outfile, inputdir + os.sep + titel + ".m4b") 

print("Check output and press ENTER to continue")
input()
exit()
